package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowDao {
    /**
     *  //保存报溢单
     * @param overflowList
     */
    void saveOverflowList(OverflowList overflowList);

    /**
     * //保存报溢单商品
     * @param overflowListGoodsList
     */
    void saveOverflowListGoods(@Param("overflowListGoodsList") List<OverflowListGoods> overflowListGoodsList);

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    List<OverflowList> getOverflowList(@Param("sTime") String sTime,@Param("eTime") String eTime);

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    List<OverflowListGoods> getOverflowGoods(@Param("overflowListId") Integer overflowListId);
}
