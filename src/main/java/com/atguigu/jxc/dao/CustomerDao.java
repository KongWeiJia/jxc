package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    /**
     * 客户列表分页（名称模糊查询）
     * @param customerName
     * @param fromIndex
     * @param rows
     * @return
     */
    List<Customer> getCustomerList(@Param("fromIndex") Long fromIndex,@Param("rows") Long rows,@Param("customerName") String customerName);

    /**
     * 客户添加
     * @param customer
     */
    void saveCustomer(Customer customer);

    /**
     * 客户添加或修改
     * @param customer
     */
    void updateCustomer(Customer customer);

    /**
     * 客户删除（支持批量删除）
     * @param idsArray
     */
    void deleteCustomer(@Param("idsArray") String[] idsArray);
}
