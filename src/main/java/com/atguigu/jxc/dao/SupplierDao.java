package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    /**
     * 分页查询供应商
     * @param fromIndex
     * @param rows
     * @param supplierName
     * @return
     */
    List<Supplier> getSupplierList(@Param("fromIndex") long fromIndex,@Param("rows") Long rows, @Param("supplierName")String supplierName);

    /**
     * 添加操作
     * @param supplier
     */
    void saveSupplier(Supplier supplier);

    /**
     * 供应商修改
     * @param supplier
     */
    void updateSupplier(Supplier supplier);

    /**
     * 删除供应商（支持批量删除）
     * @param idsList
     */
    void deleteSupplierByIds(@Param("ids") String[] idsList);
}
