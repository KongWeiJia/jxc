package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    /**
     * 分页查询商品库存信息
     *
     * @param from        从第几条开始查
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List<Goods> getGoodsList(@Param("from") long from, @Param("rows") Long rows,
                             @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 单个商品总销售量
     *
     * @param goodsCode
     * @return
     */
    Integer getSaleNum(@Param("goodsCode") String goodsCode);

    /**
     * 获取单个商品退货总量
     *
     * @param goodsCode
     * @return
     */
    Integer getReturnNum(@Param("goodsCode") String goodsCode);

    /**
     * 分页查询商品信息
     *
     * @param fromIndex          当前页
     * @param rows               每页显示条数
     * @param goodsName          商品名称
     * @param typeIdArraysArrays 商品类别ID
     * @return
     */
    List<Goods> getList(@Param("fromIndex") Long fromIndex,
                        @Param("rows") Long rows,
                        @Param("goodsName") String goodsName,
                        @Param("typeIdArraysArrays") List<Integer> typeIdArraysArrays);

    /**
     * 查询所有商品单位
     *
     * @return
     */
    List<Unit> getAllUnit();

    /**
     * 添加商品信息
     *
     * @param goods
     */
    void saveGoods(Goods goods);

    /**
     * 修改商品信息
     *
     * @param goods
     */
    void updateGoods(Goods goods);

    /**
     * 根据主键 goodsId 查询商品
     *
     * @param goodsId
     * @return
     */
    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    /**
     * 商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     *
     * @param goodsId
     */
    void deleteGoodsById(Integer goodsId);

    /**
     * 分页查询无库存商品信息
     *
     * @param fromIndex  当前下标索引
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    List<Goods> getNoInventoryQuantity(@Param("fromIndex") Long fromIndex,
                                       @Param("rows") Long rows,
                                       @Param("nameOrCode") String nameOrCode);

    /**
     * 分页查询有库存商品信息
     *
     * @param fromIndex  当前下标索引
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    List<Goods> getHasInventory(@Param("fromIndex") Long fromIndex,
                                @Param("rows") Long rows,
                                @Param("nameOrCode") String nameOrCode);

    /**
     * 添加库存、修改数量或成本价
     * @param goods
     */
    void saveStock(Goods goods);

    /**
     * 查询库存报警商品信息
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    List<Goods> getListAlarm();
}
