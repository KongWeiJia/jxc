package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageDao {
    /**
     * //保存报损订单
     * @param damageList
     * @return
     */
    Integer saveDamegeList(DamageList damageList);

    /**
     * //保存报损订单商品
     * @param damageListGoodsList
     */
    void saveDamageListGoods(@Param("damageListGoodsList") List<DamageListGoods> damageListGoodsList);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> getDamageList(@Param("sTime") String sTime, @Param("eTime") String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    List<DamageListGoods> getGoodsList(Integer damageListId);
}
