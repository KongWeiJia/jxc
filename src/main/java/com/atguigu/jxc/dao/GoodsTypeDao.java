package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    /**
     * //查询父节点pId 是不是叶子节点 如果是把 类别状态goods_type_state改为1 （0为叶子节点）
     * @param pId
     * @return
     */
    GoodsType selectGoodsTypeById(@Param("pId") Integer pId);

    /**
     * 保存新增分类
     * @param newGoodsType
     */
    void saveGoodsType(GoodsType newGoodsType);

    /**
     * 删除当前节点
     * @param goodsTypeId
     */
    void deleteGoodsType(@Param("goodsTypeId") Integer goodsTypeId);
}
