package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getSupplierList(@RequestParam("page") Long page,
                                              @RequestParam("rows") Long rows,
                                              @RequestParam(value = "supplierName",required = false) String supplierName) {
        Map<String, Object> resultMap = supplierService.getSupplierList(page, rows, supplierName);
        return resultMap;
    }

    /**
     * 供应商添加或修改
     * @param supplierId
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveOrUpdateSupplier( Supplier supplier,@RequestParam(value = "supplierId" ,required = false) Integer supplierId){
        return supplierService.saveOrUpdateSupplier(supplierId,supplier);
    }

    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplierByIds(@RequestParam("ids") String ids) {
        return supplierService.deleteSupplierByIds(ids);
    }

}
