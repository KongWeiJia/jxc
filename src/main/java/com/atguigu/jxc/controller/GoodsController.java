package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return Map<String, Object>
     */
    @PostMapping("/listInventory")
    public Map<String, Object> getGoodsList(@RequestParam("page") Long page,
                                            @RequestParam("rows") Long rows,
                                            @RequestParam(value = "codeOrName", required = false) String codeOrName,
                                            @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {
        Map<String, Object> map = goodsService.getGoodsList(page, rows, codeOrName, goodsTypeId);
        return map;
    }


    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getList(@RequestParam(value = "goodsName", required = false) String goodsName,
                                       @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId,
                                       @RequestParam("page") Long page,
                                       @RequestParam("rows") Long rows) {
        return goodsService.getList(goodsName, goodsTypeId, page, rows);
    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveGoods(Goods goods,
                               @RequestParam(value = "goodsId", required = false) Integer goodsId) {
        return goodsService.saveGoods(goods, goodsId);
    }

    /**
     * 商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteGoods(@RequestParam("goodsId") Integer goodsId) {
        return goodsService.deleteGoods(goodsId);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(@RequestParam("page") Long page,
                                                      @RequestParam("rows") Long rows,
                                                      @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        return goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
    }


    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("getHasInventoryQuantity")
    public Map<String, Object> getHasInventory(@RequestParam("page") Long page,
                                               @RequestParam("rows") Long rows,
                                               @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        return goodsService.getHasInventory(page, rows, nameOrCode);
    }


    /**
     * 添加库存、修改数量或成本价
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(@RequestParam("goodsId") Integer goodsId,
                               @RequestParam("inventoryQuantity") Integer inventoryQuantity,
                               @RequestParam("purchasingPrice") Double purchasingPrice){
        return goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    /**
     * 删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("deleteStock")
    public ServiceVO deleteStock(@RequestParam("goodsId") Integer goodsId){
        return goodsService.deleteStock(goodsId);
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String,Object> getListAlarm(){
        return goodsService.getListAlarm();
    }

}
