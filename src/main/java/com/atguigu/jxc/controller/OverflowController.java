package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowService;
import com.atguigu.jxc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowController {

    @Autowired
    private OverflowService overflowService;

    @Autowired
    private UserService userService;

    /**
     * 新增报溢单
     * @param session
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveOverflowList(HttpSession session,
                                      OverflowList overflowList,
                                      @RequestParam("overflowListGoodsStr") String overflowListGoodsStr){
        Map<String, Object> userInfoMap = userService.loadUserInfo(session);
        User user = (User) userInfoMap.get("user");
       return overflowService.saveOverflowList(user,overflowList,overflowListGoodsStr);
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getOverflowList(@RequestParam("sTime") String sTime,@RequestParam("eTime") String eTime){
        return overflowService.getOverflowList(sTime,eTime);
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String, Object> getOverflowGoods(@RequestParam("overflowListId") Integer overflowListId){
        return overflowService.getOverflowGoods(overflowListId);
    }

}
