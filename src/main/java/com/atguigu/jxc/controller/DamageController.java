package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageController {

    @Autowired
    private DamageService damageService;

    @Autowired
    private UserService userService;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveDamage(HttpSession session, DamageList damageList, @RequestParam("damageListGoodsStr") String damageListGoodsStr) {
        //用于获取用户的全信息
        Map<String, Object> userInfoMap = userService.loadUserInfo(session);
        User user = (User) userInfoMap.get("user");
        return damageService.saveDamage(user, damageList, damageListGoodsStr);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getDamageList(@RequestParam("sTime") String sTime,@RequestParam("eTime") String eTime){
        return damageService.getDamageList(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> getGoodsList(@RequestParam("damageListId") Integer damageListId){
        return damageService.getGoodsList(damageListId);
    }

}
