package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 查询所有商品单位
     * @return
     */
    @PostMapping("/list")
    public Map<String, List<Unit>> getAllUnit() {
       return goodsService.getAllUnit();
    }

}
