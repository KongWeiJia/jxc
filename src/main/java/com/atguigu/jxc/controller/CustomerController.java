package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页（名称模糊查询）
     *
     * @param customerName
     * @param page
     * @param rows
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getCustomerList(@RequestParam(value = "customerName", required = false) String customerName,
                                               @RequestParam("page") Long page,
                                               @RequestParam("rows") Long rows) {

        log.info("开发了分页接口,page:{}",page);
        System.out.println("购物车接口开发");
        return customerService.getCustomerList(customerName, page, rows);
    }

    /**
     * 客户添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    @PostMapping("save")
    public ServiceVO saveOrUpdateCustomer( Customer customer,
                                          @RequestParam(value = "customerId", required = false) Integer customerId) {
        return customerService.saveOrUpdateCustomer(customer, customerId);
    }

    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteCustomer(@RequestParam(value = "ids") String ids){
        return customerService.deleteCustomer(ids);
    }


}
