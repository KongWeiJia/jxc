package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    /**
     * 客户列表分页（名称模糊查询）
     * @param customerName
     * @param page
     * @param rows
     * @return
     */
    Map<String, Object> getCustomerList(String customerName, Long page, Long rows);

    /**
     * 客户添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    ServiceVO saveOrUpdateCustomer(Customer customer, Integer customerId);

    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    ServiceVO deleteCustomer(String ids);
}
