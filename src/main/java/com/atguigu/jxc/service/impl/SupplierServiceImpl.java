package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierList(Long page, Long rows, String supplierName) {
        long fromIndex =(page -1) * rows;
        List<Supplier> supplierList = supplierDao.getSupplierList(fromIndex,rows,supplierName);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("total",supplierList.size());
        resultMap.put("rows",supplierList);
        return resultMap;
    }

    /**
     * 供应商添加或修改
     * @param supplierId
     * @param supplier
     * @return
     */
    @Override
    public ServiceVO saveOrUpdateSupplier(Integer supplierId, Supplier supplier) {
        //如果supplierId为空 是添加操作
        if (supplierId == null){
            supplierDao.saveSupplier(supplier);
        }else {
            //修改
//            supplier.setSupplierId(supplierId);
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    @Override
    public ServiceVO deleteSupplierByIds(String ids) {
        String[] idsList = ids.split(",");
        supplierDao.deleteSupplierByIds(idsList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
