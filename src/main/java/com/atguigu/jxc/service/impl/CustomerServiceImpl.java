package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /**
     * 客户列表分页（名称模糊查询）
     * @param customerName
     * @param page
     * @param rows
     * @return
     */
    @Override
    public Map<String, Object> getCustomerList(String customerName, Long page, Long rows) {
        Long fromIndex = (page - 1) * rows;
        List<Customer> customerList = customerDao.getCustomerList(fromIndex,rows,customerName);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("total",customerList.size());
        resultMap.put("rows",customerList);
        return resultMap;
    }

    /**
     * 客户添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    @Override
    public ServiceVO saveOrUpdateCustomer(Customer customer, Integer customerId) {
        //如果customerId 为空 此操作为：新增
        if (customerId == null){
            customerDao.saveCustomer(customer);
        }else {
            customer.setCustomerId(customerId);
            customerDao.updateCustomer(customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    @Override
    public ServiceVO deleteCustomer(String ids) {
        String[] idsArray = ids.split(",");
        customerDao.deleteCustomer(idsArray);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

}
