package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowServiceImpl implements OverflowService {

    @Autowired
    private OverflowDao overflowDao;

    /**
     * 新增报溢单
     *
     * @param user
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @Override
    public ServiceVO saveOverflowList(User user, OverflowList overflowList, String overflowListGoodsStr) {
        //保存报溢单
        overflowList.setUserId(user.getUserId());
        overflowDao.saveOverflowList(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();
        //保存报溢单商品
        List<OverflowListGoods> overflowListGoodsList = JSONObject.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        overflowListGoodsList.stream().forEach((overflowListGoods) -> {
            overflowListGoods.setOverflowListId(overflowListId);
        });
        overflowDao.saveOverflowListGoods(overflowListGoodsList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        List<OverflowList> overflowLists = overflowDao.getOverflowList(sTime, eTime);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",overflowLists);
        return resultMap;
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> getOverflowGoods(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowDao.getOverflowGoods(overflowListId);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",overflowListGoodsList);
        return resultMap;
    }
}
