package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
@Slf4j
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> getGoodsList(Long page, Long rows, String codeOrName, Integer goodsTypeId) {
        long from = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsList(from, rows, codeOrName, goodsTypeId);
        //封装实际销售总数
        goodsList.stream().forEach(goods -> {
            //获取单个商品总销售量
            int saleNum = goodsDao.getSaleNum(goods.getGoodsCode()) == null ? 0 : goodsDao.getSaleNum(goods.getGoodsCode());

            //获取单个商品退货总量
            int returnNum = goodsDao.getReturnNum(goods.getGoodsCode()) == null ? 0 : goodsDao.getReturnNum(goods.getGoodsCode());

            log.info("saleNum, returnNum, {}, {}", saleNum, returnNum);
            //实际总的销售量
            int resultNum = saleNum - returnNum;
            goods.setSaleTotal(resultNum);
        });
        HashMap<String, Object> map = new HashMap<>();
        map.put("total", goodsList.size());
        map.put("rows", goodsList);
        return map;
    }

    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> getList(String goodsName, Integer goodsTypeId, Long page, Long rows) {
        List<Integer> typeIdArraysArrays = new ArrayList<>();
        if (goodsTypeId != null) {
            typeIdArraysArrays.add(goodsTypeId);
            log.info("typeIdArraysArrays:{}", typeIdArraysArrays);
            List<Integer> childTypeIdArraysArrays = this.getgoodsTypeIdArrays(goodsTypeId);
            childTypeIdArraysArrays.stream().forEach(childTypeId -> {
                typeIdArraysArrays.add(childTypeId);
            });
        }

        Long fromIndex = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getList(fromIndex, rows, goodsName, typeIdArraysArrays);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", goodsList.size());
        resultMap.put("rows", goodsList);
        return resultMap;
    }

    /**
     * 封装goodsTypeId 数组
     *
     * @param goodsTypeId
     */
    private List<Integer> getgoodsTypeIdArrays(Integer goodsTypeId) {
        ArrayList<Integer> idsTypeArrays = new ArrayList<>();
        //查询商品分类的 p_id
        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(goodsTypeId);
        //如果查询 p_id 不为空的话，说明它有子节点了
        if (!CollectionUtils.isEmpty(goodsTypeList)) {
            goodsTypeList.stream().forEach(goodsType -> {
                idsTypeArrays.add(goodsType.getGoodsTypeId());
                List<Integer> childTypeIdArrays = this.getgoodsTypeIdArrays(goodsType.getGoodsTypeId());
                childTypeIdArrays.stream().forEach(childTypeId -> {
                    idsTypeArrays.add(childTypeId);
                });
            });
        } else {
            //如果查询 p_id 为空的话，说明它已经没有子节点了
            idsTypeArrays.add(goodsTypeId);
        }
        return idsTypeArrays;
    }

    /**
     * 查询所有商品单位
     *
     * @return
     */
    @Override
    public Map<String, List<Unit>> getAllUnit() {
        List<Unit> unitList = goodsDao.getAllUnit();
        HashMap<String, List<Unit>> resultMap = new HashMap<>();
        resultMap.put("rows", unitList);
        return resultMap;
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public ServiceVO saveGoods(Goods goods, Integer goodsId) {
        //如果goodsId为空 此操作为新增
        if (goodsId == null) {
            goods.setState(0);
            goods.setInventoryQuantity(goods.getMinNum());
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            goodsDao.saveGoods(goods);
        } else {
//            goods.setGoodsId(goodsId);
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        //先查询商品判断商品状态 0表示初始值,1表示已入库，2表示有进货或销售单据
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() == 1 || goods.getState() == 2) {
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
        }
        goodsDao.deleteGoodsById(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Long page, Long rows, String nameOrCode) {
        Long fromIndex = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(fromIndex, rows, nameOrCode);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", goodsList.size());
        resultMap.put("rows", goodsList);
        return resultMap;
    }

    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getHasInventory(Long page, Long rows, String nameOrCode) {
        Long fromIndex = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getHasInventory(fromIndex, rows, nameOrCode);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", goodsList.size());
        resultMap.put("rows", goodsList);
        return resultMap;
    }

    /**
     * 添加库存、修改数量或成本价
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        Goods goods = new Goods();
        goods.setGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.saveStock(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        //先判断商品状态 // 0 初始化状态 1 期初库存入仓库  2  有进货或者销售单据
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() == 1 || goods.getState() == 2) {
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
        }
        goodsDao.deleteGoodsById(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询库存报警商品信息
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @Override
    public Map<String, Object> getListAlarm() {
        List<Goods> goodsList = goodsDao.getListAlarm();
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",goodsList);
        return resultMap;
    }

}
