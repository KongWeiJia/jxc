package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DamageServiceImpl implements DamageService {

    @Autowired
    private DamageDao damageDao;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public ServiceVO saveDamage(User user, DamageList damageList, String damageListGoodsStr) {
//        List<DamageListGoods> damageListGoodsList = JSONObject.parseArray(damageListGoodsStr, DamageListGoods.class);
//        log.info("damageListGoodsList:{}",damageListGoodsList);

        //保存报损订单
        damageList.setUserId(user.getUserId());
        Integer count = damageDao.saveDamegeList(damageList);
        Integer damageListId = damageList.getDamageListId();
        //保存报损订单商品
        List<DamageListGoods> damageListGoodsList = JSONObject.parseArray(damageListGoodsStr, DamageListGoods.class);
        damageListGoodsList.stream().forEach((damageListGoods) -> {
            damageListGoods.setDamageListId(damageListId);
        });
        damageDao.saveDamageListGoods(damageListGoodsList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getDamageList(String sTime, String eTime) {
        List<DamageList> damageLists = damageDao.getDamageList(sTime, eTime);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",damageLists);
        return resultMap;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        List<DamageListGoods> damageListGoodsList = damageDao.getGoodsList(damageListId);
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",damageListGoodsList);
        return resultMap;
    }

}
