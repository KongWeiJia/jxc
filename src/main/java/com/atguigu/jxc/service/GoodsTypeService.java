package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    ServiceVO saveGoodsType(String goodsTypeName, Integer pId);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    ServiceVO deleteGoodsType(Integer goodsTypeId);
}
