package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;

import java.util.Map;

public interface OverflowService {
    /**
     * 新增报溢单
     * @param user
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    ServiceVO saveOverflowList(User user, OverflowList overflowList, String overflowListGoodsStr);

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getOverflowList(String sTime, String eTime);

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> getOverflowGoods(Integer overflowListId);
}
