package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return Map<String,Object>
     */
    Map<String, Object> getGoodsList(Long page, Long rows, String codeOrName, Integer goodsTypeId);

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    Map<String, Object> getList(String goodsName, Integer goodsTypeId, Long page, Long rows);

    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, List<Unit>> getAllUnit();

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    ServiceVO saveGoods(Goods goods, Integer goodsId);

    /**
     * 商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     * @param goodsId 商品ID
     * @return
     */
    ServiceVO deleteGoods(Integer goodsId);

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    Map<String, Object> getNoInventoryQuantity(Long page, Long rows, String nameOrCode);

    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    Map<String, Object> getHasInventory(Long page, Long rows, String nameOrCode);

    /**
     * 添加库存、修改数量或成本价
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);

    /**
     * 删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId 商品ID
     * @return
     */
    ServiceVO deleteStock(Integer goodsId);

    /**
     * 查询库存报警商品信息
     * @return
     */
    Map<String, Object> getListAlarm();
}
