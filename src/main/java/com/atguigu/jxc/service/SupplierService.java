package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    /**
     * 分页查询供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> getSupplierList(Long page, Long rows, String supplierName);

    /**
     * 供应商添加或修改
     * @param supplierId
     * @param supplier
     * @return
     */
    ServiceVO saveOrUpdateSupplier(Integer supplierId, Supplier supplier);

    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    ServiceVO deleteSupplierByIds(String ids);
}
